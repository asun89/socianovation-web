---
title: Team
published: true
menu: Team
process:
    markdown: true
child_type: default
routable: true
cache_enable: true
visible: true
people:
    -
        name: 'Yosia Sukiman'
        pic: yosia.jpg
        position: 'Co Founder - Lead Programmer'
        social:
            -
                title: twitter
                url: 'https://www.twitter.com/yosisuki'
            -
                title: facebook
                url: 'https://www.facebook.com/yosia.sukiman'
            -
                title: linkedin
                url: 'https://id.linkedin.com/in/yosia-sukiman-6329695a'
    -
        name: 'Cindy Natalia'
        pic: cindy.jpg
        position: 'Co Founder - Lead Marketing'
        social:
            -
                title: twitter
                url: 'https://www.twitter.com/cindyntl'
            -
                title: facebook
                url: 'https://www.facebook.com/cindy.natalia'
            -
                title: linkedin
                url: 'https://id.linkedin.com/in/cindy-natalia-1b5539105'
            
description: 'We work together, sharing our goal together, and help each other to achive it.'
---

## Our Amazing Team
### Yes, we're still a small team, but we never stop growing.
