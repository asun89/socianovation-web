---
title: Portfolio
menu: portfolio
portfolios:
    -
        title: AoC United Website
        subtitle: 'www.aocunited.com'
        layout: default
        modalid: 1
        date: 1405616400
        img: aocunited.png
        thumbnail: aocunited-thumbnail.png
        alt: image-alt
        projectdate: 'January 2016'
        client: 'AoC United'
        category: 'Web Development'
        description: 'We make changes for AoC United Official Website into the new layout.'
    -
        title: Ezytravel Website
        subtitle: 'www.ezytravel.co.id'
        layout: default
        modalid: 2
        date: 1405616400
        img: ezytravel.png
        thumbnail: ezytravel-thumbnail.png
        alt: image-alt
        projectdate: 'August 2015'
        client: 'Ezytravel'
        category: 'Web Development'
        description: 'Online Travel Agent Website for Online Booking.'
    -
        title: Sistem Penjualan
        subtitle: 'Desktop Application'
        layout: default
        modalid: 3
        date: 1405616400
        img: sisjul.png
        thumbnail: sisjul-thumbnail.png
        alt: image-alt
        projectdate: 'August 2014'
        client: 'Button and Bow'
        category: 'Desktop Development'
        description: 'POS Desktop Application.'
    -
        title: Button and Bow Website
        subtitle: 'www.mybuttonandbow.com'
        layout: default
        modalid: 4
        date: 1405616400
        img: mybuttonandbow.png
        thumbnail: mybuttonandbow-thumbnail.png
        alt: image-alt
        projectdate: 'September 2014'
        client: 'Button and Bow'
        category: 'Web Development'
        description: 'Fashion E-Commerce.'
---

## Portfolio
### Our Works so far.
