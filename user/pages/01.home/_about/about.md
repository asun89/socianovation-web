---
title: About
published: true
menu: About
process:
    markdown: true
child_type: default
routable: true
cache_enable: true
visible: true
abouts:
    -
        img: 3.jpg
        heading: 'March 2014'
        subheading: 'Our Humble Beginnings.'
        body: 'We start with our first project!'
        align: image
    -
        img: 2.jpg
        heading: 2015-2016
        subheading: 'Then... Here comes the busy day'
        body: 'While we working with our current jobs, we still develop our very own mobile apps!'
        align: inverted
    -
        img: 1.jpg
        heading: 'January 2016'
        subheading: 'New Year, New Adventure.'
        body: 'We move to our next level!'
        align: image
    -
        img: null
        heading: null
        alttext: 'Be Part<br />Of Our <br />Story!'
        subheading: null
        body: null
        align: inverted
---

## About
### Our journey was just started and it still continues.
