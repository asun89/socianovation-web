<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'D:/Ampps/www/grav/user/accounts/asun89.yaml',
    'modified' => 1459509714,
    'data' => [
        'email' => 'yosia89@yahoo.com',
        'fullname' => 'Yosia Sukiman',
        'title' => 'Administrator',
        'state' => 'enabled',
        'access' => [
            'admin' => [
                'login' => true,
                'super' => true
            ],
            'site' => [
                'login' => true
            ]
        ],
        'hashed_password' => '$2y$10$1ExuxhiaXoCq2jZTi2KTGeCSIeuoACx8ZFp5v49yerYsxI..e1tx2'
    ]
];
