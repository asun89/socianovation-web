<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'plugins://form/form.yaml',
    'modified' => 1459509624,
    'data' => [
        'enabled' => true,
        'files' => [
            'multiple' => false,
            'destination' => '@self',
            'accept' => [
                0 => 'image/*'
            ]
        ]
    ]
];
