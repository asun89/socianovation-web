<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'C:/Program Files/Ampps/www/grav/user/config/site.yaml',
    'modified' => 1459959574,
    'data' => [
        'title' => 'SOCIANOVATION - Be Social, Be Innovative',
        'author' => [
            'name' => 'Yosia Sukiman',
            'email' => 'yosia@socianovation.com'
        ],
        'taxonomies' => [
            0 => 'category',
            1 => 'tag'
        ],
        'metadata' => [
            'description' => 'SOCIANOVATION is a group of developers and designers who mastered the art of web technologies to provide clients a new way to publish their business.'
        ],
        'summary' => [
            'enabled' => true,
            'format' => 'short',
            'size' => 300,
            'delimiter' => '==='
        ],
        'blog' => [
            'route' => '/blog'
        ],
        'email' => 'yosia@socianovation.com',
        'description' => 'Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.',
        'links' => [
            0 => [
                'title' => 'Services',
                'url' => '#services'
            ],
            1 => [
                'title' => 'Portfolio',
                'url' => '#portfolio'
            ],
            2 => [
                'title' => 'About',
                'url' => '#about'
            ],
            3 => [
                'title' => 'Team',
                'url' => '#team'
            ],
            4 => [
                'title' => 'Contact',
                'url' => '#contact'
            ]
        ],
        'social' => [
            0 => [
                'title' => 'twitter',
                'url' => 'http://twitter.com/socianovation'
            ],
            1 => [
                'title' => 'facebook',
                'url' => 'https://www.facebook.com/socianovation'
            ],
            2 => [
                'title' => 'github',
                'url' => 'http://github.com/socianovation'
            ]
        ],
        'address' => [
            0 => [
                'line' => 'Jl. Cendani VI Blok E No. 122'
            ],
            1 => [
                'line' => 'Pondok Bambu, Jakarta Timur 13430'
            ]
        ],
        'quicklinks' => [
            0 => [
                'title' => 'About Us',
                'url' => '#about'
            ],
            1 => [
                'title' => 'Contact Us',
                'url' => '#contact'
            ]
        ]
    ]
];
