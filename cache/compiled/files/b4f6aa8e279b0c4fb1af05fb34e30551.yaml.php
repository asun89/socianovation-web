<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'D:/Ampps/www/grav/user/plugins/admin/languages/lt.yaml',
    'modified' => 1459509648,
    'data' => [
        'PLUGIN_ADMIN' => [
            'MANAGE_PAGES' => 'Tvarkyti puslapius',
            'PAGES' => 'Puslapiai',
            'THEMES' => 'Temos',
            'BACK' => 'Atgal',
            'MOVE' => 'Perkelti',
            'DELETE' => 'Ištrinti',
            'SAVE' => 'Išsaugoti',
            'ERROR' => 'Klaida',
            'CANCEL' => 'Atšaukti',
            'CONTINUE' => 'Tęsti'
        ]
    ]
];
