<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'plugins://gmaps/gmaps.yaml',
    'modified' => 1459788260,
    'data' => [
        'enabled' => true,
        'id' => 'gmap',
        'width' => 600,
        'height' => 450,
        'type' => 'roadmap',
        'class' => 'gmap',
        'address' => '',
        'zoom' => 15
    ]
];
