<?php

/* partials/base.html.twig */
class __TwigTemplate_84cc4d73d0d27724431c37afba7c0fb182c083a5014a281fd694ffe186eb67f9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'header_navigation' => array($this, 'block_header_navigation'),
            'body' => array($this, 'block_body'),
            'content' => array($this, 'block_content'),
            'footer' => array($this, 'block_footer'),
            'javascripts_bottom' => array($this, 'block_javascripts_bottom'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
  ";
        // line 4
        $this->displayBlock('head', $context, $blocks);
        // line 25
        echo "</head>
<body id=\"page-top\" class=\"index\">

  ";
        // line 28
        $this->displayBlock('header_navigation', $context, $blocks);
        // line 31
        echo "  ";
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "
  ";
        // line 35
        $this->displayBlock('footer', $context, $blocks);
        // line 38
        echo "
  ";
        // line 39
        $this->displayBlock('javascripts_bottom', $context, $blocks);
        // line 47
        echo "  ";
        echo $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "js", array(), "method");
        echo "

</body>
</html>
";
    }

    // line 4
    public function block_head($context, array $blocks = array())
    {
        // line 5
        echo "  <meta charset=\"utf-8\" />
  <title>";
        // line 6
        if ($this->getAttribute((isset($context["header"]) ? $context["header"] : null), "title", array())) {
            echo $this->getAttribute((isset($context["header"]) ? $context["header"] : null), "title", array());
            echo " | ";
        }
        echo $this->getAttribute((isset($context["site"]) ? $context["site"] : null), "title", array());
        echo "</title>
  ";
        // line 7
        $this->loadTemplate("partials/metadata.html.twig", "partials/base.html.twig", 7)->display($context);
        // line 8
        echo "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no\">
  <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 9
        echo $this->env->getExtension('GravTwigExtension')->urlFunc("theme://img/favicon.png");
        echo "\" />

  ";
        // line 11
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 22
        echo "  ";
        echo $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "css", array(), "method");
        echo "

  ";
    }

    // line 11
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 12
        echo "    ";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addCss", array(0 => "theme://css-compiled/global.css", 1 => 100), "method");
        // line 13
        echo "    ";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addCss", array(0 => "theme://css/custom.css", 1 => 100), "method");
        // line 14
        echo "    ";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addCss", array(0 => "theme://css/font-awesome/css/font-awesome.min.css", 1 => 100), "method");
        // line 15
        echo "    <link href=\"http://fonts.googleapis.com/css?family=Montserrat:400,700\" rel=\"stylesheet\" type=\"text/css\">
    <link href='http://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href=\"http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic\" rel=\"stylesheet\" type=\"text/css\">
    <link href='https://fonts.googleapis.com/css?family=Orbitron:900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
  ";
    }

    // line 28
    public function block_header_navigation($context, array $blocks = array())
    {
        // line 29
        echo "    ";
        $this->loadTemplate("partials/navigation.html.twig", "partials/base.html.twig", 29)->display($context);
        // line 30
        echo "  ";
    }

    // line 31
    public function block_body($context, array $blocks = array())
    {
        // line 32
        echo "    ";
        $this->displayBlock('content', $context, $blocks);
        // line 33
        echo "  ";
    }

    // line 32
    public function block_content($context, array $blocks = array())
    {
    }

    // line 35
    public function block_footer($context, array $blocks = array())
    {
        // line 36
        echo "    ";
        $this->loadTemplate("partials/footer.html.twig", "partials/base.html.twig", 36)->display($context);
        // line 37
        echo "  ";
    }

    // line 39
    public function block_javascripts_bottom($context, array $blocks = array())
    {
        // line 40
        echo "    ";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "add", array(0 => "jquery", 1 => 101), "method");
        // line 41
        echo "    ";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addJs", array(0 => "theme://js/bootstrap.min.js", 1 => 100), "method");
        // line 42
        echo "    ";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addJs", array(0 => "theme://js/jquery.easing.min.js"), "method");
        // line 43
        echo "    ";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addJs", array(0 => "theme://js/classie.js"), "method");
        // line 44
        echo "    ";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addJs", array(0 => "theme://js/cbpAnimatedHeader.js"), "method");
        // line 45
        echo "    ";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addJs", array(0 => "theme://js/agency.js"), "method");
        // line 46
        echo "  ";
    }

    public function getTemplateName()
    {
        return "partials/base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  175 => 46,  172 => 45,  169 => 44,  166 => 43,  163 => 42,  160 => 41,  157 => 40,  154 => 39,  150 => 37,  147 => 36,  144 => 35,  139 => 32,  135 => 33,  132 => 32,  129 => 31,  125 => 30,  122 => 29,  119 => 28,  109 => 15,  106 => 14,  103 => 13,  100 => 12,  97 => 11,  89 => 22,  87 => 11,  82 => 9,  79 => 8,  77 => 7,  69 => 6,  66 => 5,  63 => 4,  53 => 47,  51 => 39,  48 => 38,  46 => 35,  43 => 34,  40 => 31,  38 => 28,  33 => 25,  31 => 4,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html lang="en">*/
/* <head>*/
/*   {% block head %}*/
/*   <meta charset="utf-8" />*/
/*   <title>{% if header.title %}{{ header.title }} | {% endif %}{{ site.title }}</title>*/
/*   {% include 'partials/metadata.html.twig' %}*/
/*   <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">*/
/*   <link rel="icon" type="image/png" href="{{ url('theme://img/favicon.png') }}" />*/
/* */
/*   {% block stylesheets %}*/
/*     {% do assets.addCss('theme://css-compiled/global.css',100) %}*/
/*     {% do assets.addCss('theme://css/custom.css',100) %}*/
/*     {% do assets.addCss('theme://css/font-awesome/css/font-awesome.min.css',100) %}*/
/*     <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">*/
/*     <link href='http://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>*/
/*     <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">*/
/*     <link href='https://fonts.googleapis.com/css?family=Orbitron:900' rel='stylesheet' type='text/css'>*/
/*     <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>*/
/*     <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>*/
/*   {% endblock %}*/
/*   {{ assets.css() }}*/
/* */
/*   {% endblock head%}*/
/* </head>*/
/* <body id="page-top" class="index">*/
/* */
/*   {% block header_navigation %}*/
/*     {% include 'partials/navigation.html.twig' %}*/
/*   {% endblock %}*/
/*   {% block body %}*/
/*     {% block content %}{% endblock %}*/
/*   {% endblock %}*/
/* */
/*   {% block footer %}*/
/*     {% include 'partials/footer.html.twig' %}*/
/*   {% endblock %}*/
/* */
/*   {% block javascripts_bottom %}*/
/*     {% do assets.add('jquery',101) %}*/
/*     {% do assets.addJs('theme://js/bootstrap.min.js',100) %}*/
/*     {% do assets.addJs('theme://js/jquery.easing.min.js') %}*/
/*     {% do assets.addJs('theme://js/classie.js') %}*/
/*     {% do assets.addJs('theme://js/cbpAnimatedHeader.js') %}*/
/*     {% do assets.addJs('theme://js/agency.js') %}*/
/*   {% endblock %}*/
/*   {{ assets.js() }}*/
/* */
/* </body>*/
/* </html>*/
/* */
