<?php

/* modular/contact.html.twig */
class __TwigTemplate_b4f59ac3b764c8eb12c381dd871c9585650e6ede743f1834ce03eaa279d46a57 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section id=\"contact\">
  <div class=\"container\">
    <div class=\"row\">
      <div class=\"col-lg-12 text-center\">
       ";
        // line 5
        echo (isset($context["content"]) ? $context["content"] : null);
        echo "
\t   
     </div>
   </div>
   <div class=\"row\">
    ";
        // line 10
        if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["config"]) ? $context["config"] : null), "plugins", array()), "simple_form", array()), "enabled", array())) {
            // line 11
            echo "        ";
            $context["simple_form_config"] = (($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "simple_form", array())) ? ($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "simple_form", array())) : ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["config"]) ? $context["config"] : null), "themes", array()), "agency", array()), "simple_form", array())));
            // line 12
            echo "        ";
            echo call_user_func_array($this->env->getFunction('simple_form')->getCallable(), array((isset($context["simple_form_config"]) ? $context["simple_form_config"] : null)));
            echo "
    ";
        }
        // line 14
        echo "    </div>
  </div>
</section>
";
    }

    public function getTemplateName()
    {
        return "modular/contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 14,  38 => 12,  35 => 11,  33 => 10,  25 => 5,  19 => 1,);
    }
}
/* <section id="contact">*/
/*   <div class="container">*/
/*     <div class="row">*/
/*       <div class="col-lg-12 text-center">*/
/*        {{ content }}*/
/* 	   */
/*      </div>*/
/*    </div>*/
/*    <div class="row">*/
/*     {% if config.plugins.simple_form.enabled %}*/
/*         {% set simple_form_config = page.header.simple_form ?: config.themes.agency.simple_form %}*/
/*         {{ simple_form(simple_form_config) }}*/
/*     {% endif %}*/
/*     </div>*/
/*   </div>*/
/* </section>*/
/* */
